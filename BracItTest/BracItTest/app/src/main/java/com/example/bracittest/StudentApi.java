package com.example.bracittest;


import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;

public interface StudentApi {


    @GET("students")
    Call<List<StudentPojo>> getStudentList();


    @POST("student")
    Call<StudentPojo> setStudent(@Body StudentPojo studentPojo);


    @POST("student/update")
    Call<StudentPojo> updateStudent(@Body StudentPojo studentPojo);

    @POST("deletestudent/{id}")
    Call<StudentPojo> deleteStudent(@Path("id") int id);

}
