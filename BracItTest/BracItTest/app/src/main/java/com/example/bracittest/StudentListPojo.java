package com.example.bracittest;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class StudentListPojo {
    @SerializedName("studentpojo")
    @Expose
    private List<StudentPojo> studentPojos = null;

    public List<StudentPojo> getStudentPojos() {
        return studentPojos;
    }

    public void setStudentPojos(List<StudentPojo> studentPojos) {
        this.studentPojos = studentPojos;
    }
}
