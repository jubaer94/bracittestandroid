package com.example.bracittest;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Toast;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class StudentAddActivity extends AppCompatActivity {

    private EditText etName, etDetails;
    private Button mAdd;
    private RadioButton mMale, mFemale;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_student_add);
         StudentPojo studentPojo = new StudentPojo();
        etName = findViewById(R.id.et_name);
        etDetails = findViewById(R.id.et_detail);
        mAdd = findViewById(R.id.btn_add_student);
        mMale = findViewById(R.id.rb_male);
        mFemale = findViewById(R.id.rb_female);


        mAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(isNetworkConnected()){
                    addStudent();
                }else{
                    Toast.makeText(StudentAddActivity.this, "No Internet Connection", Toast.LENGTH_SHORT).show();
                }
            }
        });


    }

    public void addStudent() {

        Retrofit retrofit = NetworkClient.getRetrofitClient();
        StudentApi studentApi = retrofit.create(StudentApi.class);
        Call<StudentPojo> call = studentApi.setStudent(new StudentPojo(etName.getText().toString(), getGender(),etDetails.getText().toString(), System.currentTimeMillis()));
        call.enqueue(new Callback<StudentPojo>() {
            @Override
            public void onResponse(@NonNull Call<StudentPojo> call, @NonNull Response<StudentPojo> response) {
                /*This is the success callback. Though the response type is JSON, with Retrofit we get the response in the form of WResponse POJO class
                 */
                if (response.isSuccessful()) {

                    StudentPojo studentPojo =  new StudentPojo();
                    studentPojo.setId(response.body().getId());
                    studentPojo.setName(response.body().getName());
                    studentPojo.setGender(response.body().getGender());
                    studentPojo.setDescription(response.body().getDescription());
                    AppDatabase database = AppDatabase.getDatabase(StudentAddActivity.this);
                    database.studentDao().insertAll(studentPojo);
                    finish();
                }
            }

            @Override
            public void onFailure(@NonNull Call<StudentPojo> call, Throwable t) {
                /*
                Error callback
                */
                Log.e("STUDENTS","Error"+t.toString());
            }
        });
    }

    private String getGender(){

        if(mMale.isChecked()){
            return "Male";
        }else{
            return "Female";
        }
    }
    private boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        return cm.getActiveNetworkInfo() != null && cm.getActiveNetworkInfo().isConnected();
    }
}