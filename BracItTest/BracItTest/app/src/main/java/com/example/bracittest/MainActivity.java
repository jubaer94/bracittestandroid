package com.example.bracittest;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import androidx.appcompat.app.AppCompatActivity;


public class MainActivity extends AppCompatActivity {

    private Button mBtnList, mBtnAddStudent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mBtnAddStudent = findViewById(R.id.btn_add_student);
        mBtnList = findViewById(R.id.btn_student_list);

        mBtnList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, StudentListActivity.class);
                startActivity(intent);
            }
        });
        mBtnAddStudent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, StudentAddActivity.class);
                startActivity(intent);
            }
        });
        addDemoData();
    }

    private void addDemoData() {
        if (Constants.DEMO) {
            AppDatabase db = AppDatabase.getDatabase(this);

            StudentPojo studet1 = new StudentPojo("Hasan", "Male", "Regular", System.currentTimeMillis());
            StudentPojo studet2 = new StudentPojo("Rahman", "Male", "Regular", System.currentTimeMillis());
            StudentPojo studet3 = new StudentPojo("Shuva", "Female", "Regular", System.currentTimeMillis());

            db.studentDao().insertAll(studet1);
            db.studentDao().insertAll(studet2);
            db.studentDao().insertAll(studet3);
        }
    }


}