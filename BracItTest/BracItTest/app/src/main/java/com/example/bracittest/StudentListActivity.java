package com.example.bracittest;

import android.content.Context;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class StudentListActivity extends AppCompatActivity {

    private List<StudentPojo> mStudentPojos = new ArrayList<>();
    private RecyclerView recyclerView;
    private StudentsAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_student_list);

        initView();
        if(isNetworkConnected()){
            fetchStudentList();
        }else{
        fetchStudentListFromLocalDB();
        }
    }

    private void initView() {
        recyclerView = findViewById(R.id.rv_student_list);
        adapter = new StudentsAdapter(this);
        LinearLayoutManager layoutManager
                = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);

        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);

    }

    private void fetchStudentListFromLocalDB() {
        AppDatabase database = AppDatabase.getDatabase(this);
        mStudentPojos.clear();
        mStudentPojos.addAll(database.studentDao().getAll());
        adapter.setData(mStudentPojos);
        recyclerView.getAdapter().notifyDataSetChanged();
    }

    public void fetchStudentList() {

        Retrofit retrofit = NetworkClient.getRetrofitClient();
        StudentApi studentApi = retrofit.create(StudentApi.class);
        Call<List<StudentPojo>> call = studentApi.getStudentList();
        call.enqueue(new Callback<List<StudentPojo>>() {
            @Override
            public void onResponse(@NonNull Call<List<StudentPojo>> call, @NonNull Response<List<StudentPojo>> response) {
                /*This is the success callback. Though the response type is JSON, with Retrofit we get the response in the form of WResponse POJO class
                 */
                if (response.isSuccessful()) {

                    if (response.body() != null) {
                        mStudentPojos.clear();
                        mStudentPojos.addAll(response.body());
                        adapter.setData(mStudentPojos);
                        recyclerView.getAdapter().notifyDataSetChanged();
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<List<StudentPojo>> call, Throwable t) {
                /*
                Error callback
                */
                Log.e("STUDENTS", "Error" + t.toString());
            }
        });
    }

    private boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        return cm.getActiveNetworkInfo() != null && cm.getActiveNetworkInfo().isConnected();
    }
}