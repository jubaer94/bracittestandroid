package com.example.bracittest;

import android.content.Context;
import android.net.ConnectivityManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class StudentsAdapter extends RecyclerView.Adapter<StudentsAdapter.ViewHolder> {

    private List<StudentPojo> studentPojos = new ArrayList<>();
    private Context context;

    public StudentsAdapter(Context context) {
        this.context = context;
    }

    public void setData(List<StudentPojo> studentPojos) {
        this.studentPojos.clear();
        this.studentPojos.addAll(studentPojos);
    }

    // Create new views (invoked by the layout manager)
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        // Create a new view, which defines the UI of the list item
        View view = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.student_row_item, viewGroup, false);

        return new ViewHolder(view);
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(ViewHolder viewHolder, final int position) {

        // Get element from your dataset at this position and replace the
        // contents of the view with that element
        if (studentPojos != null && studentPojos.size() > 0) {
            viewHolder.name.setText(studentPojos.get(position).getName());
            viewHolder.gender.setText(studentPojos.get(position).getGender());
            viewHolder.details.setText(studentPojos.get(position).getDescription());

            viewHolder.mUpdate.setOnClickListener(view -> {
                if (isNetworkConnected()) {
                    update(position , viewHolder.name.getText().toString() , viewHolder.gender.getText().toString(),viewHolder.details.getText().toString());
                } else {
                    Toast.makeText(context, "No Internet Connection", Toast.LENGTH_SHORT).show();
                }
            });

            viewHolder.mDelete.setOnClickListener(view -> {
                if (isNetworkConnected()) {
                    delete(position);
                } else {
                    Toast.makeText(context, "No Internet Connection", Toast.LENGTH_SHORT).show();
                }
            });
        }
    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return studentPojos.size();
    }

    private void update(int position , String name , String gender , String details) {
        Retrofit retrofit = NetworkClient.getRetrofitClient();
        StudentApi studentApi = retrofit.create(StudentApi.class);
        Call<StudentPojo> call = studentApi.updateStudent(new StudentPojo(studentPojos.get(position).getId(), name , gender ,details ,   System.currentTimeMillis()));
        call.enqueue(new Callback<StudentPojo>() {
            @Override
            public void onResponse(@NonNull Call<StudentPojo> call, @NonNull Response<StudentPojo> response) {
                /*This is the success callback. Though the response type is JSON, with Retrofit we get the response in the form of WResponse POJO class
                 */
                if (response.isSuccessful()) {
                    Toast.makeText(context, "Update Successfull", Toast.LENGTH_SHORT).show();
                    StudentPojo studentPojo = new StudentPojo();
                    studentPojo.setId(response.body().getId());
                    studentPojo.setName(response.body().getName());
                    studentPojo.setGender(response.body().getGender());
                    studentPojo.setDescription(response.body().getDescription());
                    AppDatabase database = AppDatabase.getDatabase(context);
                    database.studentDao().update(studentPojo);

                }
            }

            @Override
            public void onFailure(@NonNull Call<StudentPojo> call, Throwable t) {
                /*
                Error callback
                */
                Log.e("STUDENTS", "Error" + t.toString());
            }
        });

    }

    private void delete(int position) {
        Retrofit retrofit = NetworkClient.getRetrofitClient();
        StudentApi studentApi = retrofit.create(StudentApi.class);
        Call<StudentPojo> call = studentApi.deleteStudent(studentPojos.get(position).getId());
        call.enqueue(new Callback<StudentPojo>() {
            @Override
            public void onResponse(@NonNull Call<StudentPojo> call, @NonNull Response<StudentPojo> response) {
                if (response.isSuccessful()) {
                    Toast.makeText(context, "delete Successfull", Toast.LENGTH_SHORT).show();
                    studentPojos.remove(position);
                    notifyDataSetChanged();
                }
                /*This is the success callback. Though the response type is JSON, with Retrofit we get the response in the form of WResponse POJO class
                 */
            }

            @Override
            public void onFailure(@NonNull Call<StudentPojo> call, Throwable t) {
                /*
                Error callback
                */
                Log.e("STUDENTS", "Error" + t.toString());
            }
        });
    }

    private boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        return cm.getActiveNetworkInfo() != null && cm.getActiveNetworkInfo().isConnected();
    }

    /**
     * Provide a reference to the type of views that you are using
     * (custom ViewHolder).
     */
    public static class ViewHolder extends RecyclerView.ViewHolder {
        private final EditText name, gender, details;
        private Button mUpdate, mDelete;

        public ViewHolder(View view) {
            super(view);
            // Define click listener for the ViewHolder's View

            name = view.findViewById(R.id.et_name);
            gender = view.findViewById(R.id.et_gender);
            details = view.findViewById(R.id.et_detals);
            mUpdate = view.findViewById(R.id.btn_update);
            mDelete = view.findViewById(R.id.btn_delete);

        }
    }
}
