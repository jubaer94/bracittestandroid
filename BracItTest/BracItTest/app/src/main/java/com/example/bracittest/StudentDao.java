package com.example.bracittest;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

@Dao
public interface StudentDao {

    @Query("SELECT * FROM studentpojo")
    List<StudentPojo> getAll();

    @Query("SELECT * FROM studentpojo WHERE id IN (:id)")
    List<StudentPojo> loadAllByIds(int[] id);

    @Insert
    void insertAll(StudentPojo... studentPojos);

    @Delete
    void delete(StudentPojo studentPojos);

    @Update
    void update(StudentPojo studentPojos);
}
