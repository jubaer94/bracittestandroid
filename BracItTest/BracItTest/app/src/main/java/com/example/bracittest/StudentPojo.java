package com.example.bracittest;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import com.google.gson.annotations.SerializedName;

@Entity
public class StudentPojo {

    @PrimaryKey(autoGenerate = true)
    @SerializedName("id")
    private int id;

    @SerializedName("name")
    @ColumnInfo(name = "name")
    private String name;

    @SerializedName("gender")
    @ColumnInfo(name = "gender")
    private String gender;

    @SerializedName("description")
    @ColumnInfo(name = "description")
    private String description;

    @SerializedName("birthDate")
    @ColumnInfo(name = "birthDate")
    private long birthDate;

    public StudentPojo() {
    }

    public StudentPojo(String name, String gender, String description, long birthDate) {
        this.name = name;
        this.gender = gender;
        this.description = description;
        this.birthDate = birthDate;
    }

    public StudentPojo(int id, String name, String gender, String description, long birthDate) {
        this.id = id;
        this.name = name;
        this.gender = gender;
        this.description = description;
        this.birthDate = birthDate;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public long getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(long birthDate) {
        this.birthDate = birthDate;
    }
}